Sample web app for signalR

[![DotNetFramework46](https://img.shields.io/badge/.Net-Framework%204.6-green.svg)](https://en.wikipedia.org/wiki/.NET_Framework)
[![CSharp](https://img.shields.io/badge/Language-C%23-green.svg)](https://en.wikipedia.org/wiki/C_Sharp_(programming_language))

Changes

- Startup.cs - app.MapSignalR();
- HomeController.cs - Chat action
- Chat.cshtml
- ChatHub.cs
